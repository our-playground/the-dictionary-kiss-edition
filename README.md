The dictionary database cannot be uploaded here that's why you need to download it manually.
Download the dictionary here (dictionary.json): https://github.com/matthewreagan/WebstersEnglishDictionary/blob/master/dictionary.json
And put it in the root directory of app.py. All done!